import { lazy } from 'react';

// Not Lazy Load (10.73s, 190 kB)
// import Counter from '../components/Counter';
// import CreateNewPlayer from '../components/CreateNewPlayer';
// import RatingComponent from '../components/RatingComponent'
// import Suit from '../components/Suit';

// Lazy Loaded (12.63s, 190 kB)
const Counter = lazy(() => import('../components/Counter'));
const CounterClassComponent = lazy(() => import('../components/CounterClassComponent'));
const CreateNewPlayer = lazy(() => import('../components/CreateNewPlayer'));
const RatingComponent = lazy(() => import('../components/RatingComponent'));
const Suit = lazy(() => import('../components/Suit'));

const routes = [
    {
        path: '/rating',
        component: <RatingComponent stars={3} bebasdeh={"hehe"} />,
        exact: false,
    },
    {
        path: '/suit',
        component: <Suit />,
        exact: false,
    },
    {
        path: '/counter',
        component: <Counter />,
        exact: false,
    },
    {
        path: '/counter-class',
        component: <CounterClassComponent />,
        exact: false,
    },
    {
        path: '/',
        component: <CreateNewPlayer />,
        exact: true,
    },
    {
        path: '*',
        component: "Not Found",
        exact: false,
    },
]

export default routes;