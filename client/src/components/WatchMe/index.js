import React, { useEffect, useState } from 'react';
// PascalCase
const WatchMe = (props) => {
    const { watchElement } = props;
    const [offset, setOffset] = useState({
        x: 0,
        y: 0,
    });
    useEffect(() => {
        watchElement.current.addEventListener("mousemove", (e) => {
            let followX = (window.innerWidth / 2 - e.clientX);
            let followY = (window.innerHeight / 2 - e.clientY);
    
            let x = 0,
                y = 0;
            x +=( (-followX - x) * (1 / 8));
            y += (followY - y) * (1 / 8);
            setOffset({x, y})
        });
    }, [])
    let offsetStyle = {
        transform: `perspective(600px)
                    rotateY(${offset.x}deg)
                    rotateX(${offset.y}deg)`
    }
    return React.cloneElement(props.children, {
        style: offsetStyle
    })
}

export default WatchMe;