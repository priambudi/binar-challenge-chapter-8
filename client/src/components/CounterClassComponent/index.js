import React, { Component } from 'react';
import { Fab } from '@mui/material';
// PascalCase
class CounterClassComponent extends Component {
    state = {
        count: 0
    }

    // 5 nextnya 1. 1 sebelumnya 5
    handleAdd = () => {
        // props didapet dari this.props;
        const { count } = this.state;
        if (count === 5) {
            this.setState({ count: 1 })
        } else {
            this.setState({ count: count + 1 })
        }
    }
    handleSub = () => {
        const { count } = this.state;
        if (count <= 0) {
            this.setState({ count: 0 })
        } else {
            this.setState({ count: count - 1 })
        }
    }
    render() {
        const { count } = this.state;
        return <>
            <h2>{count}</h2>
            <Fab
                onClick={this.handleAdd}
                color="primary"
                aria-label="add"
            >
                Tambah
            </Fab>
            <Fab
                onClick={this.handleSub}
                color="primary"
                aria-label="add"
                disabled={count === 0}
            >
                Kurang
            </Fab>
        </>
    }
}

export default CounterClassComponent;